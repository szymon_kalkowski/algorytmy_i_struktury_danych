import random
import math
from timeit import default_timer as timer

def randomList(n):
    list=[]
    for i in range(n):
        list.append(random.randint(-1000,1001))
    return list

def quickSort(A,p,r):
    if p<r :
        q = partition(A,p,r)
        quickSort(A,p,q)
        quickSort(A,q+1,r)
    return A

def partition(A,p,r):
    x=A[r] # element wyznaczajacy podział (piwot)
    i=p-1
    for j in range (p, r+1):
        if A[j]<=x :
            i=i+1
            A[i], A[j] = A[j], A[i]
    if i<r :
        return i
    else:
        return i-1  

A=[]
f1 = open("numbers.txt")
for i in f1:
    A.append(int(i))
f1.close()
A=quickSort(A,0,len(A)-1)
f2 = open("sorted_numbers_qs.txt", "w")
for j in A:
    f2.write(str(j)+"\n")
f2.close()


size=[10,50,100,500]
avg_rand=[]
avg_bad=[]

for n in size:
    avg1=0
    avg2=0
    for i in range(n):
        x=randomList(n)
        start1 = timer()
        quickSort(x,0,len(x)-1)
        stop1 = timer()
        Tn1=stop1-start1
        avg1+=Tn1
        
        x=quickSort(x,0,len(x)-1)

        start2 = timer()
        quickSort(x,0,len(x)-1)
        stop2 = timer()
        Tn2=stop2-start2
        avg2+=Tn2
    avg1/=n
    avg2/=n
    avg_rand.append(avg1)
    avg_bad.append(avg2)

k1=" rozmiar tablicy N "
k2=" (średni) czas - dane losowe "
k3=" czas - dane niekorzystne "
print(k1, "|", k2, "|", k3)
for i in range(len(size)):
    print(" "+str(size[i])+" "*(len(k1)-len(str(size[i])))+"| "+str(avg_rand[i])+" "*(len(k2)+1-len(str(avg_rand[i])))+"| "+str(avg_bad[i]))



""" 
 rozmiar tablicy N  |  (średni) czas - dane losowe  |  czas - dane niekorzystne 
 10                 | 1.4760000000000467e-05        | 1.0379999999998723e-05
 50                 | 6.301200000000007e-05         | 0.0001711919999999996
 100                | 0.00014367600000000265        | 0.0006570010000000022
 500                | 0.0009994134000000153         | 0.01547785719999996 
 
"""