class Node:
    def __init__(self,k):
        self.key = k
        self.next = None
        self.prev = None 

x = Node(7)
y = Node(6)
z = Node(5)
a = Node(6)
b = Node(6)

class LinkedList:
    def __init__(self):
        self.head = Node(None)

    def listInsert(self, x): 
    # wstawia wezel x do listy L
    # lista z wartownikiem
        help = self.head
        x.next = self.head.next
        if self.head.next != None:
            self.head.next.prev = x
        self.head.next = x
        x.prev = help

    def listSearch(self,k):
    # szuka wezla zawierajacego klucz k
    # lista z wartownikiem
        x = self.head.next
        while x!=None and x.key!=k:
            x = x.next
        return x # wynik “none” oznacza, ze szukanego klucza nie ma na liscie 
    def listDelete(self,x):
    # usuwa wezel x z listy
    # lista z wartownikiem
        x=self.listSearch(x)
        x.prev.next = x.next
        x.next.prev = x.prev
    def listPrint(self):
        list = []
        x=self.head
        while x.next!=None:
            x=x.next
            list.append(x.key)
        print(list)
    def noRedundant(self):
        list=[]
        x=self.head
        while x.next!=None:
            if (x.next.key not in list): 
                list.append(x.next.key)
            x=x.next
        return list
        

def merge(l1, l2):
    merged=LinkedList()
    x=l1.head
    y=l2.head
    z=merged.head
    z.next=x.next
    x.next.prev=z
    x.next=None
    while z.next!=None:
        z=z.next
    z.next=y.next
    y.next.prev=z
    y.next=None
    return merged

new_list = LinkedList()

new_list.listPrint()

new_list.listInsert(x)
new_list.listInsert(y)
new_list.listInsert(z)
new_list.listInsert(a)
new_list.listInsert(b)

new_list.listPrint()

print(new_list.listSearch(5))
print(new_list.listSearch(2))

print(new_list.noRedundant())

new_list.listDelete(5)

new_list.listPrint()


n1=Node(1)
n2=Node(2)
n3=Node(3)
n4=Node(4)

l1 = LinkedList()
l1.listInsert(n2)
l1.listInsert(n1)
l1.listPrint()

l2 = LinkedList()
l2.listInsert(n4)
l2.listInsert(n3)
l2.listPrint()

l3=merge(l1,l2)
l3.listPrint()

l1.listPrint()
l2.listPrint()