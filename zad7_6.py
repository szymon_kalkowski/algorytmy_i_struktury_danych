class Node: 
    def __init__(self,x,m): 
        self.key = x 
        self.m = m #odczyt
        self.left = None # lewy syn 
        self.right = None # prawy syn 
        self.p = None # ojciec

def earlier(x, y): #([dzień, miesiąc],[dzień, miesiąc])
    if x[1] < y[1]:
        return True
    elif x[1] > y[1]:
        return False
    else: 
        if x[0] < y[0]:
            return True
        else:
            return False

class BST: 
    def __init__(self): 
        self.root = None

    def BSTsearch(self,k): 
    # szuka wezla zawierajacego klucz k 
        x = self.root 
        while x!=None and x.key!=k: 
            #if k<x.key: 
            if earlier(k,x.key):
                x=x.left 
            else: 
                x=x.right 
        return x # None oznacza, ze szukanego klucza nie ma w drzewie

    def BSTinsert(self, z, m): 
    # wstawia wezel z do drzewa 
        z=Node(z, m)
        x = self.root 
        y = None # y jest ojcem x 
        while x != None: 
            y=x 
            #if z.key<x.key:
            if earlier(z.key, x.key): 
                x=x.left 
            else: 
                x=x.right 
        z.p=y 
        if y==None: # drzewo puste 
            self.root=z 
        else: 
            #if z.key<y.key:
            if earlier(z.key, y.key): 
                y.left=z 
            else: 
                y.right=z

    def bstMinimum(self,x): 
    # zwraca skrajny lewy węzeł w poddrzewie o korzeniu x czyli węzeł o najmniejszym kluczu w tym poddrzewie 
        while x.left != None: 
            x = x.left 
        return x

    def bstMaximum(self,x): 
    # zwraca skrajny prawy węzeł w poddrzewie o korzeniu x czyli węzeł o największym kluczu w tym poddrzewie 
        while x.right != None: 
            x = x.right
        return x

    def bstDelete(self,z): 
    # usuwa węzeł"z" z drzewa 
    # wersja "naturalna" 
        if z.left==None and z.right==None: # (1) z liść 
            if z==self.root: 
                self.root=None 
            else: 
                if z==z.p.left: # z jest lewym synem 
                    z.p.left=None 
                else: 
                    z.p.right=None 
        elif z.left != None and z.right != None: # (3) dwóch synów 
            y=self.bstMinimum(z.right) 
            z.key=y.key
            z.m=y.m # przepisz zawartość węzła y do z 
            self.bstDelete(y) # przypadek (1) lub (2)
        else: # (2) jeden syn 
            if z.left != None: # z.right==None 
                z.left.p=z.p 
                if z==self.root: 
                    self.root=z.left 
                else: 
                    if z==z.p.left: 
                        z.p.left=z.left 
                    else: 
                        z.p.right=z.left 
            else: # z.left==None 
                z.right.p=z.p 
                if z==self.root: 
                    self.root=z.right 
                else: 
                    if z==z.p.left: 
                        z.p.left=z.right
                    else: 
                        z.p.right=z.right

    def BSTinOrder(self,x):
    # przechodzi i drukuje klucze poddrzewa 
    # o korzeniu "x" w kolejnosci 
    # "wewnętrznej (in order)"
        if x==None: return
        self.BSTinOrder(x.left)
        print(str(x.key)+" "+str(x.m))
        self.BSTinOrder(x.right)

    def bstValue(self,x):
        x=self.BSTsearch(x)
        if x.m!=None:
            print(x.m)
        else:
            if x.right!=None and x.left!=None and self.bstMinimum(x.right)!=None and self.bstMaximum(x.left)!=None:
                print((self.bstMinimum(x.right).m + self.bstMaximum(x.left).m)/2)
            elif x.right!=None and self.bstMinimum(x.right).right!=None and self.bstMinimum(x.right)!=None and self.bstMinimum(self.bstMinimum(x.right).right)!=None:
                print(2*self.bstMinimum(x.right).m-self.bstMinimum(self.bstMinimum(x.right).right).m)
            elif x.left!=None and self.bstMaximum(x.left).left!=None and self.bstMaximum(x.left)!=None and self.bstMaximum(self.bstMaximum(x.left).left)!=None:
                print(2*self.bstMaximum(x.left).m-self.bstMaximum(self.bstMaximum(x.left).left).m)
            else:
                print("Brak wartości i niewystarczająca ilość danych do oszacowania wyniku.")

    def bstValues(self,x,y):
        z=x
        while earlier(z,y): 
            if self.BSTsearch(z)!=None:
                print(self.BSTsearch(z).m)
            z[0]+=1
            if z[0] > 31:
                z[0]=1
                z[1]+=1

def merge(t1, t2):
    def copy(w=t1.root):
        if w.left!=None:
            copy(w.left)
        if w.right!=None:
            copy(w.right)
        t2.BSTinsert(w.key, w.m)
        t1.bstDelete(w)
    while t1.root!=None:
        copy()


drzewo = BST()

drzewo.BSTinsert([6,2], 6)
drzewo.BSTinsert([7,3], None)
drzewo.BSTinsert([21,5], 7)
drzewo.BSTinsert([30,3], 8)
drzewo.BSTinsert([1,12], 5)
drzewo.BSTinsert([27,7], 6)
drzewo.BSTinsert([17,5], 4)
drzewo.BSTinOrder(drzewo.root)
drzewo.bstValue([7,3])
print("")
drzewo.bstDelete(drzewo.BSTsearch([30,3]))
drzewo.BSTinOrder(drzewo.root)
print("")

drzewo2 = BST()
drzewo2.BSTinsert([23,5], 6)
drzewo2.BSTinsert([13,8], 5)
drzewo2.BSTinsert([12,10], 7)
drzewo2.BSTinsert([5,11], 9)

merge(drzewo2, drzewo)
drzewo.BSTinOrder(drzewo.root)
print("")

drzewo.BSTinOrder(drzewo2.root)

drzewo.bstValues([6,2],[1,12])