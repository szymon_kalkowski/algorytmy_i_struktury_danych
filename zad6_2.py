#indeks 278823
#print((2+7+8+8+2+3)%3) --> 0 [W+OL] 

import math

m=int(input("Podaj rozmiar m: "))
procent=int(input("Podaj procent wypełnienia tablicy do symulacji: "))

f1 = open("nazwiskaASCII.txt")
A=[]
for i in f1:
    x=i[:-1].split(" ")
    A.append((int(x[0]), x[1]))
f1.close()

def h1(k):
    return hash(k)%m

def h(k,i):
    return (h1(k[1])+i)%m

T = [[] for i in range(m)]

def wstaw_elem(x):
    for i in range(m*m):
        if T[h(x,i)] == [] or T[h(x,i)] == ['del']:
            T[h(x,i)].append(x)
            break

def wstaw(x):
    for i in range(x):
        wstaw_elem(A[i])

def wstaw_sym(x):
    j=1
    for i in range(m*m):
        if T[h(x,i)] == [] or T[h(x,i)] == ['del']:
            return j
        j+=1

def symulacja(x):
    j=0
    sum=0
    for i in range(x,x+math.floor(5*m/100)+1):
        sum+=wstaw_sym(A[i])
        j+=1
    return sum/j
        
iter=math.floor(m/100*procent)
wstaw(iter)
print("Średnia liczba prób: "+str(round(symulacja(iter),2)))

"""
Podaj rozmiar m: 4447
Podaj procent wypełnienia tablicy do symulacji: 35
Średnia liczba prób: 1.73

Podaj rozmiar m: 4447
Podaj procent wypełnienia tablicy do symulacji: 65
Średnia liczba prób: 4.75

Podaj rozmiar m: 4447
Podaj procent wypełnienia tablicy do symulacji: 95
Średnia liczba prób: 105.92

Podaj rozmiar m: 11177
Podaj procent wypełnienia tablicy do symulacji: 35 
Średnia liczba prób: 1.71

Podaj rozmiar m: 11177
Podaj procent wypełnienia tablicy do symulacji: 65
Średnia liczba prób: 4.62

Podaj rozmiar m: 11177
Podaj procent wypełnienia tablicy do symulacji: 95
Średnia liczba prób: 257.27
"""
