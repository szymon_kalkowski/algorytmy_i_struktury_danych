import random
import math
from timeit import default_timer as timer

#GENEROWANIE LOSOWYCH MACIERZY KWADRATOWYCH N-TEGO STOPNIA

def losowa_macierz(n):
    macierz = [ [ random.randint(0,1) for i in range(n) ] for j in range(n) ]
    return macierz

def macierz_zerowa(n):
    macierz = [ [ 0 for i in range(n) ] for j in range(n) ]
    return macierz

def macierz_jedynkowa(n):
    macierz = [ [ 1 for i in range(n) ] for j in range(n) ]
    return macierz

#PIERWSZY ALGORYTM

def najwieksza_podmacierz(macierz):
    podmacierz=[[]]
    for i in range(0, len(macierz)):
        for j in range(0, len(macierz)):
            for k in range(1, len(macierz)+1):
                for l in range(1, len(macierz)+1):
                    if i<=k and j<=l:
                        x=1
                        for a in range(i,k):
                            for b in range(j,l):
                                x*=macierz[a][b]
                        if x==1 and ((k-i)*(l-j))>(len(podmacierz)*len(podmacierz[0])):
                            podmacierz = [ [ 1 for w in range(l-j) ] for z in range(k-i) ]
    return podmacierz

#DRUGI ALGORYTM

def najwieksza_podmacierz2(macierz):
    def rzad(nr_rzedu,jj,macierz):
        prawo=0
        tab=[]
        global lim
        while jj+prawo != len(macierz) and macierz[nr_rzedu][jj+prawo] == 1 and jj+prawo<lim:
            tab.append(1)
            prawo+=1
        if jj+prawo<lim:
            lim=jj+prawo
        return tab
    podmacierz2=[[]]
    for i in range(len(macierz)):
        for j in range(len(macierz)):
            if macierz[i][j] == 1:
                ksztalt=[]
                dol=0
                global lim
                lim=len(macierz)
                while ((i+dol) != len(macierz) and macierz[i+dol][j] == 1):
                    ksztalt.append(rzad(i+dol,j,macierz))
                    dol+=1
                """ for s in range(0, len(ksztalt)):
                    print(ksztalt[s])
                print("\n")  """         
                for _ in range(0, len(ksztalt)):
                    szerokosc=len(ksztalt[_])
                    dlugosc=1
                    while _+dlugosc != len(ksztalt) and len(ksztalt[_+dlugosc])>=szerokosc:
                        dlugosc+=1
                    pole=dlugosc*szerokosc
                    if len(podmacierz2)*len(podmacierz2[0])<pole:
                        podmacierz2 = [ [ 1 for w in range(szerokosc) ] for z in range(dlugosc) ]
                
                for _ in reversed(range(len(ksztalt))):
                    szerokosc=len(ksztalt[_])
                    dlugosc=1
                    while _-dlugosc != -1 and (len(ksztalt[_-dlugosc]))>=szerokosc:
                        dlugosc+=1
                    pole=dlugosc*szerokosc
                    if len(podmacierz2)*len(podmacierz2[0])<pole:
                        podmacierz2 = [ [ 1 for w in range(szerokosc) ] for z in range(dlugosc) ]
    return podmacierz2


#SPRAWDZENIE CZY OBIE FUNKCJE ZWRACAJĄ TO SAMO

macierz1=losowa_macierz(5)

for i in range(0, len(macierz1)):
    print(macierz1[i])

print("\n")

podmacierz1=najwieksza_podmacierz(macierz1)
podmacierz2=najwieksza_podmacierz2(macierz1)

print("\n")
for i in range(0, len(podmacierz1)):
    print(podmacierz1[i])

print("\n")
for i in range(0, len(podmacierz2)):
    print(podmacierz2[i])


#TEST PIERWSZEJ FUNKCJI DLA MACIERZY LOSOWYCH

""" for n in range(20,41):  
  mx=losowa_macierz(n)
  start = timer()
  najwieksza_podmacierz(mx)
  stop = timer()
  Tn=stop-start
  Fn=n**5.45
  print(n, '\t', format(Tn, '.8f'), '\t', int(Fn/Tn)) """

""" 
20       0.21317330      57793722
21       0.27216010      59056907
22       0.35077910      59043201
23       0.44098880      59839664
24       0.55281200      60196855
25       0.68907940      60325895
26       0.84913050      60622006
27       1.05053090      60189908
28       1.28150860      60157495
29       1.55824770      59900796
30       1.87949890      59740487
31       2.26002780      59402873
32       2.70762590      58949309
33       3.17554270      59440576
34       3.75087450      59214431
35       4.40946010      58990954
36       5.16678990      58698611
37       6.02446610      58449504
38       7.02961070      57928105
39       8.10361080      57892663
40       9.37230530      57461971 
"""

#TEST DRUGIEJ FUNKCJI DLA MACIERZY LOSOWYCH

""" for n in range(200,301,5):  
  mx=losowa_macierz(n)
  start = timer()
  najwieksza_podmacierz2(mx)
  stop = timer()
  Tn=stop-start
  Fn=n**2
  print(n, '\t', format(Tn, '.8f'), '\t', int(Fn/Tn)) """


""" 
200      0.07449060      536980
205      0.07893680      532387
210      0.08279360      532649
215      0.08557320      540180
220      0.08880940      544987
225      0.09269390      546152
230      0.10039180      526935
235      0.10132810      545011
240      0.10722200      537203
245      0.10978580      546746
250      0.11816590      528917
255      0.12146760      535327
260      0.12648750      534440
265      0.13528900      519073
270      0.14046810      518979
275      0.14437490      523809
280      0.14961690      524004
285      0.15317250      530284
290      0.16362000      513995
295      0.16789370      518333
300      0.17165810      524298 
"""


#TEST PIERWSZEJ FUNKCJI DLA MACIERZY ZEROWEJ 

""" for n in range(20,41):  
  mx=macierz_zerowa(n)
  start = timer()
  najwieksza_podmacierz(mx)
  stop = timer()
  Tn=stop-start
  Fn=n**5.45
  print(n, '\t', format(Tn, '.8f'), '\t', int(Fn/Tn)) """

""" 
20       0.20052610      61438777
21       0.25889680      62082396
22       0.33198450      62385807
23       0.42288220      62401826
24       0.53153680      62606284
25       0.65740160      63232782
26       0.81898530      62853380
27       0.99835900      63335292
28       1.22188410      63093011
29       1.48680170      62779237
30       1.78871430      62772562
31       2.14267860      62656221
32       2.55313860      62516260
33       3.02687690      62360015
34       3.56353030      62327490
35       4.20717780      61827256
36       4.91482240      61707904
37       5.73623300      61386463
38       6.66996560      61051594
39       7.69287110      60983683
40       8.87139230      60706496 
"""

#TEST DRUGIEJ FUNKCJI DLA MACIERZY ZEROWEJ

""" for n in range(200,301,5):  
  mx=macierz_zerowa(n)
  start = timer()
  najwieksza_podmacierz2(mx)
  stop = timer()
  Tn=stop-start
  Fn=n**2
  print(n, '\t', format(Tn, '.8f'), '\t', int(Fn/Tn)) """

""" 
200      0.00178600      22396416
205      0.00190020      22116093
210      0.00197010      22384650
215      0.00206190      22418642
220      0.00231300      20925205
225      0.00266780      18976310
230      0.00260800      20283742
235      0.00244570      22580447
240      0.00258100      22316931
245      0.00275770      21766327
250      0.00279190      22386188
255      0.00292870      22202683
260      0.00322590      20955392
265      0.00319960      21948056
270      0.00326810      22306538
275      0.00350090      21601588
280      0.00357410      21935592
285      0.00371500      21864064
290      0.00402120      20914154
295      0.00401110      21696043
300      0.00411230      21885562 
"""

#TEST PIERWSZEJ FUNKCJI DLA MACIERZY JEDYNKOWEJ

""" for n in range(20,41):  
  mx=macierz_jedynkowa(n)
  start = timer()
  najwieksza_podmacierz(mx)
  stop = timer()
  Tn=stop-start
  Fn=n**5.45
  print(n, '\t', format(Tn, '.8f'), '\t', int(Fn/Tn)) """

""" 
20       0.20948010      58812643
21       0.26734080      60121514
22       0.34787760      59535656
23       0.43531370      60619782
24       0.54617690      60928142
25       0.67686520      61414491
26       0.83545240      61614515
27       1.02103310      61928804
28       1.24772970      61786096
29       1.51305120      61690098
30       1.82422840      61550505
31       2.18253680      61511972
32       2.59557430      61494166
33       3.09765480      60935159
34       3.72527720      59621308
35       4.29649830      60541921
36       4.98014280      60898533
37       5.83491950      60348228
38       6.78056890      60055732
39       7.78386640      60270769
40       8.96879470      60047215 
"""

#TEST DRUGIEJ FUNKCJI DLA MACIERZY JEDYNKOWEJ

""" for n in range(20,61,2):  
  mx=macierz_jedynkowa(n)
  start = timer()
  najwieksza_podmacierz2(mx)
  stop = timer()
  Tn=stop-start
  Fn=n**3.8
  print(n, '\t', format(Tn, '.8f'), '\t', int(Fn/Tn)) """

""" 
20       0.02002220      4389369
22       0.02749140      4592078
24       0.03807130      4615353
26       0.05143220      4630880
28       0.06803710      4639324
30       0.08672120      4730809
32       0.11091520      4726926
34       0.13934650      4737222
36       0.17393870      4715777
38       0.21318110      4725295
40       0.26070360      4695490
42       0.31310030      4706128
44       0.37378840      4704292
46       0.44394560      4689753
48       0.52464520      4664993
50       0.61236910      4667375
52       0.71184460      4660446
54       0.83692470      4575198
56       0.97593360      4504994
58       1.10133730      4561482
60       1.26506840      4517110 
"""