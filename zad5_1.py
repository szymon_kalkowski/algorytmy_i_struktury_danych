while True:
    w=input("Podaj wariant funkcji haszującej (w-wbudowana, d-dobra, s-słaba): ")
    m=int(input("Podaj rozmiar listy T: "))
    if w in ['w', 'd', 's']:
        break

T = [[] for i in range(m)]

def liczbaB(k, const):
    if len(k)==1:
        return ord(k[0])*const
    elif len(k)==2:
        return ord(k[0])*const+ord(k[1])
    else:
        result=ord(k[0])
        for i in range(1,len(k)-1):
            result=result*const+ord(k[i])
        return result

def hW(k):
    return hash(k) % m

def hD(k):
    return liczbaB(k,111) % m

def hS(k):
    return ord(k[0]) % m

f1 = open("3700.txt")
A=[]
for i in f1:
    A.append(i[:-1])
f1.close()

if w=="w":
    for i in range(2*m):
        T[hW(A[i])].append(A[i])
elif w=="d":
    for i in range(2*m):
        T[hD(A[i])].append(A[i])
elif w=="s":
    for i in range(2*m):
        T[hS(A[i])].append(A[i])

puste=0
max=len(T[0])
suma=0
ilosc=0
for i in T:
    #print(str(i))
    if i==[]:
        puste+=1
    if len(i)>max:
        max=len(i)
    if i!=[]:
        suma+=len(i)
        ilosc+=1
srednia=round(suma/ilosc, 2)

print("\nliczba pustych list w tablicy T: "+str(puste))
print("maksymalna długość listy w T: "+str(max))
print("średnia długość niepustych list w T: "+str(srednia))


""" 
Podaj wariant funkcji haszującej (w-wbudowana, d-dobra, s-słaba): w
Podaj rozmiar listy T: 17

['useful', 'listed', 'links', 'which']
[]
['same']
['the', 'are', 'der']
['linked']
['gcc', 'actually']
['most', 'options', 'accepts']
['see']
['remain', 'may']
['GCC']
['below', 'mostly', 'Apple', 'named']
['compiler']
[]
['GNU', 'and', 'here', 'symbolic', 'changed']
['for', 'version']
['Only', 'both', 'like']
['project']

liczba pustych list w tablicy T: 2
maksymalna długość listy w T: 5
średnia długość niepustych list w T: 2.27 

###################################################################

Podaj wariant funkcji haszującej (w-wbudowana, d-dobra, s-słaba): d
Podaj rozmiar listy T: 17

liczba pustych list w tablicy T: 1
maksymalna długość listy w T: 7
średnia długość niepustych list w T: 2.12

###################################################################

Podaj wariant funkcji haszującej (w-wbudowana, d-dobra, s-słaba): s
Podaj rozmiar listy T: 17

liczba pustych list w tablicy T: 2
maksymalna długość listy w T: 5
średnia długość niepustych list w T: 2.27

###################################################################

Podaj wariant funkcji haszującej (w-wbudowana, d-dobra, s-słaba): w
Podaj rozmiar listy T: 1031

liczba pustych list w tablicy T: 109
maksymalna długość listy w T: 7
średnia długość niepustych list w T: 2.24

###################################################################

Podaj wariant funkcji haszującej (w-wbudowana, d-dobra, s-słaba): d
Podaj rozmiar listy T: 1031

liczba pustych list w tablicy T: 145
maksymalna długość listy w T: 8
średnia długość niepustych list w T: 2.33

###################################################################

Podaj wariant funkcji haszującej (w-wbudowana, d-dobra, s-słaba): s
Podaj rozmiar listy T: 1031

liczba pustych list w tablicy T: 981
maksymalna długość listy w T: 183
średnia długość niepustych list w T: 41.24

###################################################################

Podaj wariant funkcji haszującej (w-wbudowana, d-dobra, s-słaba): w
Podaj rozmiar listy T: 1024

liczba pustych list w tablicy T: 145
maksymalna długość listy w T: 7
średnia długość niepustych list w T: 2.33

###################################################################

Podaj wariant funkcji haszującej (w-wbudowana, d-dobra, s-słaba): d
Podaj rozmiar listy T: 1024

liczba pustych list w tablicy T: 158
maksymalna długość listy w T: 8
średnia długość niepustych list w T: 2.36

###################################################################

Podaj wariant funkcji haszującej (w-wbudowana, d-dobra, s-słaba): s
Podaj rozmiar listy T: 1024

liczba pustych list w tablicy T: 974
maksymalna długość listy w T: 183
średnia długość niepustych list w T: 40.96

###################################################################

P.1
Lepsze wyniki dawał rozmiar 1031, bo jest liczbą pierwszą.

P.2
Najlepsze wyniki dawała wbudowana funkcja haszująca, następnie
dobra funkcja, a najgorzej wypadła słaba funkcja haszująca.

"""