def heapify(A, heapSize, i): 
    while True:
        l = 2*i+1 # lewy syn 
        r = 2*i+2 # prawy syn 
        if l < heapSize and A[l] < A[i]: 
            smallest = l 
        else: 
            smallest = i 
        if r < heapSize and A[r] < A[smallest]: 
            smallest = r
        if smallest !=i:  
            A[i], A[smallest] = A[smallest], A[i]
            i=smallest   
        else:
            break
    return A

def buildHeap(A):
    heapSize = len(A) 
    k = int((len(A)-2)/2) # ojciec ostatniego węzła
    for i in range(k, -1, -1): 
        heapify(A, heapSize, i) 
    return A 

def heapSort(A): 
    A = buildHeap(A) 
    heapSize = len(A) 
    for i in range(len(A)-1, 0, -1): 
        A[0], A[heapSize-1] = A[heapSize-1], A[0] 
        heapSize -= 1 
        heapify(A,heapSize,0) 
    return A

A=[]
f1 = open("numbers.txt")
for i in f1:
    A.append(int(i))
f1.close()
A=heapSort(A)
f2 = open("sorted_numbers.txt", "w")
for j in A:
    f2.write(str(j)+"\n")
f2.close()